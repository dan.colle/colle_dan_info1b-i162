# README - bonus
## Table des matières
[toc]
***
## A propos de "bonus"

A l'attention de Monsieur Maccaud,

Dans ce dossier je me suis permis d'ajouter ma petite touche personnelle d'informaticien flémard dans le but d'automatiser l'utilisation la base de données, ainsi que pousser plus loin certains sujets abordés en cours.

Dans ce répertoire, vous trouverez :

- Un fichier **install_requirements.bat** permettant l'installation des librairies requisent. (ce-dernier se trouve dans **bonus\Scripts\install_requirements.bat**)
- Un fichier **SQLinsert.bat** permettant de se connecter aux serveur SQL et bases de données, ainsi que d'y insérer des données dans colle_dan_info1b_contacts.t_personnes
- Un fichier **QRgenerator.bat** permettant la génération de codes QR personnalisés.

N'est pas dans le répertoire :
- Un script de modification d'exif (Linux uniquement, voilà pourquoi il n'y figure pas), j'ai cru comprendre que vous n'utilisez pas linux, mais je vous l'envoie volontiers sur demande !

## Prérequis

- [Windows Os](https://www.microsoft.com/fr-ch/windows?r=1) (XP, 7, 10, 11)
- [python](https://www.python.org/downloads/) version 2 ou 3

### Installation des prérequis

Un script est programmé pour installer les librairies python requises :
Exécutez le fichier **bonus\Scripts\install_requirements.bat**


### A savoir sur les prérequis
Voici la liste exacte des prérequis nécéssaires :

|Nom   |Description|Installation|
|------|-----------|------------|
|qrcode|Permet de générer des QRcodes|``pip install qrcode``
|Pillow|Permet de générer des fichiers .png|``pip install Pillow``
|mysql.connector|Permet de se connecter aux bases de données|``pip install mysql.connector``
|mysql-connector-python|Permet de se connecter aux bases de données en sha256 | ``mysql-connector-python``
|termcolor|Permet de coloriser les éléments retournés par la fonction print()|``pip install termcolor``