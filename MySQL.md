# Commandes SQL
[toc]
***
## CREATE
### Databases
Créer une base de données :
```sql
CREATE DATABASE db_name ;
```
A condition qu'elle ne soie pas déjà existante :
```
CREATE TABLE IF NOT EXISTS bd_name ;
```

### Tables
Créer une table :
```sql
CREATE TABLE table_name (
    column1 datatype,
    column2 datatype,
    column3 datatype,
);
```
***

## DROP
La commande DROP permet de supprimer (bases de sonnées, tables, colonnes, etc...)

### Databases
Supprimer une base de données :
```sql
DROP DATABASE db_name ;
```
Supprimer une base de données à condition qu'elle soie éjà existante :
```
DROP DATABASE IF EXISTS db_name ;
```
### Tables
Supprimer une table :
```sql
DROP TABLE table_name;
```
A condition que cette dernière soie déjà existante :
```
DROP TABLE IF EXISTS table_name ;
```
***

## ALTER

La commande alter permet de **modifier les colonnes d'une table précise**. 
Voici ci-dessous quelques exemples d'utilisation de la commande ALTER 

### Add column
Pour ajouter une colonne
```sql
ALTER TABLE table_name
ADD column_name datatype;
```
### Drop column
Pour supprimer une colonne
```sql
ALTER TABLE table_name
DROP COLUMN column_name;
```
### Modify column
Pour modifier le type de données qui sont contennus 
```sql
ALTER TABLE table_name
ALTER COLUMN column_name datatype;
```
***
## Instert

L'insertion de données dans une table s'effectue à l'aide de la commande suivante :
```sql
INSERT INTO table_name (column1, column2, column3)
VALUES (value1, value2, value3);
```
***
## Backup
```sql
BACKUP DATABASE db_name
TO DISK = 'filepath'
```
***
# Clés primaires et Clés étrangères

La Notion de clés primaires et étrangères est essentiel à la compréhension de **bases de données relationnelles**

**La Clé Etrangère** (Foreign Key), aussi appellée **FK** se réfère à une clé primaire

**La Clé Primaire** (Primary Key), aussi appelée **PK** rend les champs d'une colonne accessibles en-tant que références par les clés étrangères


## Ajouter / Définir  une Clé
Il existe plusieurs méthodes pour définir une clé...
Les voici :

**En ajoutant une contrainte à une table déjà existant :**

```sql
ALTER TABLE table_name
ADD CONSTRAINT PK_name PRIMARY KEY (id, other_PK_names)
ADD CONSTRAINT FK_PersonOrder
FOREIGN KEY (FK_name) REFERENCES t_name(PK_name)
```

**Au moment de créer une table** :

```sql
CREATE TABLE t_name (
	id, INT NOT_NULL AUTO_INCREMENT,
	name, VARCHAR(50),
	PRIMARY KEY (id),
	FOREIGN KEY (name) REFERENCES t_Other_Table(PK_OtherPrimaryKey)
);
```

***
# Types de données

## Différents Types de données

|Type de donnée  | Description |
|----------------|-------------|
|INT             |
|AUTO_INCREMENT  | S'incrémente automatiquement à chaque                            nouvelle ligne 
|NOT NULL        | Ne peut pas être nulle
|VARCHAR(size)   | Variable de type caractérielle pouvant contenir                    des lettres, nombres et caractères spéciaux
|BINARY          | Variable de type Binaire (par défaut, 1)
|BLOB(size)		    |	BLOBs (Binary Large Objects) max 65,535 bytes
|LONGBLOB        | Pour les BLOBS de maximum 4,294,967,296 bytes
|MEDIUMBLOB      | Pour les BLOBs de maximum 16,777,215 bytes
|TINYBLOB        | Pour les BLOBs de maximum 255 bytes
|DATE            | Pour enregistrer une date
|YEAR            | Contient une année
***

## Différentes nomination de dates ( EUR, US )

Voici le format utilisé en europe : **JJ-MM-YYYY**
Voici le format utilisé dans les pays anglophones :  **YYYY-MM-JJ**


***
# Requêtes SQL


## SELECT

Select est une commande permettant d'interroger une base de données SQL. Voici quelques exemples

```sql
SELECT * FROM `table_name`;
```
***