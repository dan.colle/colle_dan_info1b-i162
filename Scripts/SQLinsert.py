import mysql.connector
import getpass
import os
from termcolor import colored, cprint

input_host = input('Host (127.0.0.1) : ')
if input_host == '':
  input_host = '127.0.0.1'

input_user = input('Username (root) : ')
if input_user == '':
  input_user = 'root'

input_password = getpass.getpass(prompt='Mot de passe : ', stream=None) 

mydb = mysql.connector.connect(
  host=input_host,
  user=input_user,
  password=input_password
)

mycursor = mydb.cursor()
os.system("cls")
print(colored("Connected !", "yellow"))
print("Liste des bases de donnees :\n")
mycursor.execute("show databases")
myresult = mycursor.fetchall()

for x in myresult:
    print(x)

print('\n')

input_database = input('Choisir une base de données (colle_dan_info1b_contacts) : ')
if input_database == '':
  input_database = 'colle_dan_info1b_contacts'

utiliser_base = "use {}"
mycursor.execute(utiliser_base.format(input_database))


if input_database == 'colle_dan_info1b_contacts':
  os.system("cls")
  print("Ce script à été conçu pour cette base de donnée\nVous pouvez-donc y insérer un nouvel utilisateur, le souhaitez-vous ?")
  vouloir_nouvel_user = input('[Y/n] : ')

  if vouloir_nouvel_user == 'Y' or vouloir_nouvel_user == 'y':
    new_entry_prenom = input('Indiquez le prenom : ')
    new_entry_nom = input('Indiquez le nom : ')
    new_entry_adresse = input("Indiquez l'adresse : ")
    os.system("cls")
    print("Liste des genres :\n\n[1] Homme\n[2] Femme\n[3] Autre\n")
    new_entry_gender = input('Sélectionne un genre : ')
    if new_entry_gender == '1' or new_entry_gender == '2' or new_entry_gender == '3':
      sql = "INSERT INTO t_personnes (prenom_personne, nom_personne, adresse) VALUES (%s, %s, %s)"
      val = (new_entry_prenom, new_entry_nom, new_entry_adresse)
      mycursor.execute(sql, val)
      mydb.commit()
      print(mycursor.rowcount, "record inserted.")
    else:
      print("Veuillez sélectionne un genre 1,2 ou 3")
else:
  print("Il est préferable d'utiliser se script avec la base de données 'colle_dan_info1b_contacts' !")     

    
print("A bientôt !")