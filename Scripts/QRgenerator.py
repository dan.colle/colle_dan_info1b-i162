import qrcode
import os
from termcolor import colored, cprint

print("---------------\n QR Generator\n---------------\n")
message = input('Entrez une donnee : ')

os.system("cls")
print("-----------\n Couleurs\n-----------\n\n- white     - blue\n- black     - brown\n- red       - orange\n- yellow    - purple\n- green     - pink\n- gery      - cyan\n\n")
fill_color = input('Couleur du QR : ')
back_color = input('Couleur de fond : ')


os.system("cls")
print("-------------------\n Nommer le fichier\n-------------------\n")

print(colored("L'extension (.png) est automatique.\n", "yellow"))

quest_nom_fichier = input('Entrez le nom du fichier : ')

data = message

qr = qrcode.QRCode(version = 1,
                   box_size = 10,
                   border = 5)
  
qr.add_data(data)
  
qr.make(fit = True)
img = qr.make_image(fill_color = fill_color,
                    back_color = back_color)
nom_fichier = 'QR\{}.png'  
img.save(nom_fichier.format(quest_nom_fichier))